# Fixation functions of all graphs of order 7 or less and friends

This repository contains the output of the [C++ program](https://bitbucket.org/geodynapp/phasetransition) of [Evolutionary regime transitions in structured populations](https://doi.org/10.1101/361337).

The filenames are of the form ```[ID].[extension]``` where

* ```ID``` is a number associated to the graph as in [this paper](https://doi.org/10.1007/978-3-319-56154-7_20) from which one could recover the graph itself as [in here](https://bitbucket.org/snippets/geodynapp/AppyX/get-edge-list-from-uint64_t-and-back). There are some special graphs named as their families, like the $\ell$-like graphs, friendship cycles or friendship stars and some graphs of order 8.

* ```extension``` can be one of the following:
	* ```log```: Then the file contains the log of the program above when it runs on that particular graph. It show the automorphism group used (that is an argument) and the main steps of the process with timestamps.
	* ```txt```: The file contains the fixation function itself. Contains 2 lines, the first is the numerator and the second de denominator of the function. Each line is the sequence of rational coefficients starting from the lowest degree on r (that is, 0)
	* ```sage```: The file contains the fixation function itself as symbolic expression useable in any CAS software
	* ```sagediff.json``` and ```sagediff2.json```: contains information on the sign of the first and second derivative of the function on $r=1$ and the roots, if any, for $r\geq 1$